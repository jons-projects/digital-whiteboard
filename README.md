# Digital Whiteboard

The goal here is to provide a repeatable way to overlay a digital whiteboard on your webcam.  This can be done with any device that can stream onto your desktop, like an iPad or a wacom, but I'm using a reMarkable.

Example Output | My Hardware Input
:-------------:|:------------------:
![image of example whiteboard on webcam](images/capture.png) | ![image of re:markable input](images/hardware.png) 

[toc]

## Install steps
I've included my config file, but it has some issues and I'm not sure the best way to make this setup able to be deployed somewhere other than my desktop... but that's not the goal of this project, although it would be nice.

You can do this to:
1. Stream re:markable to your desktop.
    - [Own a remarkable](https://remarkable.com/store/remarkable-2/)
    - [Download desktop app](https://support.remarkable.com/hc/en-us/articles/360002665378-Desktop-app)
    - [Enable streaming](https://support.remarkable.com/hc/en-us/articles/4403721327377-Screen-Share)
1. [Install OBS.](https://obsproject.com/)
1. [Install Background Remove plugin](https://obsproject.com/forum/resources/background-removal-portrait-segmentation.1260/)
1. Find an appropriate background.  Use your imagination.
1. Add your webcam as a source (Video Capture Device).
1. Remove your background.  [This video covers it nicely along with some ideal settings](https://www.youtube.com/watch?v=0DD2RNcP0KQ)
1. Insert and crop your re:markable stream.  This is a new source with type Window Capture.
1. Optional: remove the background from your remarkable and adjust your scene to taste.  
1. Once you have everything setup, click "Start Virtual Camera", to enable your new virtual camera to be used with your app of choice.
1. Have fun.

### Other comments

- This project was intented to get me familiar with GitLab, but there wasn't really any coding required.  So it's a bit lackluster...
- Make sure you test with a friend.  Some webconference software auto-reverse your webcam for you but not for other participants!
- Consider https://about.gitlab.com/company/culture/all-remote/collaboration-and-whiteboarding/



